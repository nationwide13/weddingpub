import React, { Component } from 'react';
import { HashRouter as Router, Route } from 'react-router-dom';
import './App.css';

import AWSAppSyncClient from "aws-appsync";
import { ApolloProvider } from "react-apollo";
import { Rehydrated } from "aws-appsync-react";

import appSyncConfig from "./AppSync";
import Home from './components/home';
// import Items from './components/items';
// import CreateItem from './components/createItem';

const App = () => (
  <Home/>
);

const client = new AWSAppSyncClient({
  url: appSyncConfig.graphqlEndpoint,
  region: appSyncConfig.region,
  auth: {
    type: appSyncConfig.authenticationType,
    apiKey: appSyncConfig.apiKey,
  },
  disableOffline: true
});

const WithProvider = () => (
  <ApolloProvider client={client}>
    <Rehydrated>
      <Router>
        <App/>
      </Router>
    </Rehydrated>
  </ApolloProvider>
);

export default WithProvider;

import gql from "graphql-tag";
import Schema from './schema';

const createInvitation = gql`
mutation($input: CreateInvitationInput!) {
  createInvitation(input: $input) {
    ...InvitationReturn
  }
}${Schema.fragments.InvitationReturn}`;
const updateInvitation = gql`
mutation($input: UpdateInvitationInput!) {
  updateInvitation(input: $input) {
    ...InvitationReturn
  }
}${Schema.fragments.InvitationReturn}`;
export {
  createInvitation,
  updateInvitation
}




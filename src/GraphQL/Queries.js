import gql from "graphql-tag";
import Schema from './schema';

const getInvitation = gql`
query($id: ID!) {
  getInvitation(id: $id) {
    ...InvitationReturn
  }
}${Schema.fragments.InvitationReturn}`;
const listInvitations = gql`
query {
  listInvitations {
    ...InvitationReturn
  }
}${Schema.fragments.InvitationReturn}`;
const getNamedCount = gql(`
query {
  getNamedCount
}`);
const getPlusOneCount = gql(`
query {
  getPlusOneCount
}`);
const getKidsCount = gql(`
query {
  getKidsCount
}`);
const getGrandTotalCount = gql(`
query {
  getGrandTotalCount
}`);
const getAcceptedCount = gql(`
query {
  getAcceptedCount
}`);
export {
  getInvitation,
  listInvitations,
  getNamedCount,
  getPlusOneCount,
  getKidsCount,
  getGrandTotalCount,
  getAcceptedCount
}

import gql from "graphql-tag";

export default {fragments: {
  InvitationReturn: gql`
    fragment InvitationReturn on Invitation {
      accepted
      acceptedCount
      city
      count
      guests {
        first
        last
      }
      id
      kids
      label
      name
      plusOne
      responded
      side
      state
      street1
      street2
      zip
    }`
}}

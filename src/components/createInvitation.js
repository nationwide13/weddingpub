import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { Link } from "react-router-dom";

import * as Utilities from '../utilities';

import * as Mutations from '../GraphQL/Mutations';
import * as  Queries from '../GraphQL/Queries';
console.log(Mutations);
class CreateInvitation extends Component {

  emptyGuest = {first: '', last: ''}

  getDefaultItem = () => {
    return {
      name: '',
      count: 1,
      side: 1,
      guests: [{...this.emptyGuest}],
      plusOne: false,
      kids: 0,
    }
  };

  addGuest() {
    const item = this.state.item;
    item.guests = [
      ...item.guests,
      {...this.emptyGuest}
    ];
    item.count = item.guests.length;
    this.setState({item});
  }

  handleGuestChange(field, index, event) {
    const value = event.target.value.trim();
    const guests = this.state.item.guests;
    guests[index][field] = value;
    this.setState({
      item: {
        ...this.state.item,
        guests
      }
    });
  }

  handleFieldChange(field, event) {
    const item = this.state.item;
    if (field === 'date') {
      const date = Utilities.formatDate(event.target.value);
      item.date = date.int;
      this.setState({
        item,
        formattedDate: date.str
      });
    } else {
      if (event.target.type === 'text') {
        item[field] = event.target.value;
      } else if (event.target.type === 'checkbox') {
        item[field] = event.target.checked;
      } else {
        item[field] = event.target.value;
      }
      this.setState({item});
    }
  }

  constructor(props) {
    super(props);
    const date = Utilities.formatDate(new Date());
    const item = this.getDefaultItem();
    this.state = {
      item: {
        ...item,
        ...this.props.item
      },
      formattedDate: date.str,
      edit: (this.props.item !== undefined),
      loading: false,
      error: false,
      complete: false
    }
  }

  submitForm = () => {
    const item = this.state.item;
    this.setState({
      loading: true,
      complete: false
    });
    if (this.state.item.id) {
      this.props.updateInvitation(item).then(() => {
        this.setState({
          loading: false,
          complete: true
        });
      }).catch(() => {
        this.setState({
          loading: false,
          error: true
        });
      });
    } else {
      this.props.createInvitation(item).then((newItem) => {
        this.setState({
          loading: false,
          newItem: newItem.data.createInvitation,
          item: this.getDefaultItem(),
          complete: true
        });
      }).catch(() => {
        this.setState({
          loading: false,
          error: true
        });
      });
    }
  };

  render() {
    if (!this.props.createInvitation) {
      return null;
    }
    return (
      <div className="create-item input-form">
        <label>Name: <input type="text" value={this.state.item.name} onChange={this.handleFieldChange.bind(this, 'name')}/></label>
        <label>Kids: <input type="number" value={this.state.item.kids} onChange={this.handleFieldChange.bind(this, 'kids')}/></label>
        <label>Plus One: <input type="checkbox" value={this.state.item.plusOne} onChange={this.handleFieldChange.bind(this, 'plusOne')}/></label>
        <label>Side: <select onChange={this.handleFieldChange.bind(this, 'side')} value={this.state.item.side}>
            <option value={0}>Lyss</option>
            <option value={1}>Alex</option>\
          </select>
        </label>
        {this.state.item.guests.map((guest, index) => {
          return (
            <div key={index}>
              <label>First: <input type="text" value={guest.first} onChange={this.handleGuestChange.bind(this, 'first', index)}/></label>
              <label>last: <input type="text" value={guest.last} onChange={this.handleGuestChange.bind(this, 'last', index)}/></label>
            </div>
          );
        })}
        {this.state.edit &&
          <div>
            <label>Street1: <input type="text" value={this.state.item.street1 ? this.state.item.street1 : ''} onChange={this.handleFieldChange.bind(this, 'street1')}/></label>
            <label>Street2: <input type="text" value={this.state.item.street2 ? this.state.item.street2 : ''} onChange={this.handleFieldChange.bind(this, 'street2')}/></label>
            <label>City: <input type="text" value={this.state.item.city ? this.state.item.city : ''} onChange={this.handleFieldChange.bind(this, 'city')}/></label>
            <label>Zip: <input type="text" value={this.state.item.zip ? this.state.item.zip : ''} onChange={this.handleFieldChange.bind(this, 'zip')}/></label>
            <label>State: <input type="text" value={this.state.item.state ? this.state.item.state : ''} onChange={this.handleFieldChange.bind(this, 'state')}/></label>
          </div>
        }
        <button disabled={this.state.error || this.state.loading} type="button" onClick={this.addGuest.bind(this)}>Add Guest</button>
        <button disabled={this.state.error || this.state.loading} type="button" onClick={this.submitForm.bind(this)}>{this.state.edit ? 'Edit' : 'Create'} Invitation</button>
        {this.state.complete && <div>{this.state.edit ? 'Update' : 'Create'} invitation complete!</div>}
        {this.state.newItem && <Link to={'/edit/' + this.state.newItem.id}>Edit New Item</Link>}
      </div>
    );
  }

}

export default compose(
  graphql(
    Mutations.createInvitation,
    {
      options: {
        refetchQueries: [{ query: Queries.listInvitations }],
        update: (proxy, {data: { createInvitation }}) => {
          const query = Queries.listInvitations;
          const data = proxy.readQuery({ query });
          if (createInvitation) {
            data.listInvitations.push(createInvitation);
          }
          proxy.writeQuery({ query, data });
        }
      },
      props: ({mutate}) => ({
        createInvitation: (input) => {
          return mutate({
            variables: {input: input}
          })
        }
      })
    }
  ),
  graphql(
    Mutations.updateInvitation,
    {
      options: {
        refetchQueries: [{ query: Queries.listInvitations }],
        update: (proxy, info) => {
          const updateInvitation = info.data.updateInvitation;
          const query = Queries.listInvitations;
          const data = proxy.readQuery({ query });
          data.listInvitations = [...data.listInvitations.filter(e => e.id !== updateInvitation.id), updateInvitation];
          proxy.writeQuery({ query, data });
        }
      },
      props: ({mutate}) => ({
        updateInvitation: (input) => {
          delete input.__typename;
          const guests = input.guests;
          input.guests = [];
          for (let i = 0; i < guests.length; i++) {
            if (guests[i].first !== "" && guests[i].last !== "") {
              input.guests.push({first: guests[i].first, last: guests[i].last});
            }
          }
          return mutate({
            variables: {input: input}
          })
        }
      })
    }
  )
)(CreateInvitation);

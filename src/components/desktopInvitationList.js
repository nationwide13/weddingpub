import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class DestopInvitationList extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    if (!this.props.invitations) {
      return null;
    }
    return (
      <table>
        <thead>
        <tr>
          <th><input placeholder="Name" type="text" value={this.props.nameFilter} onChange={(event) => this.props.filterChange('nameFilter', event.target.value)}/></th>
          <th><input placeholder="First" type="text" value={this.props.firstFilter} onChange={(event) => this.props.filterChange('firstFilter', event.target.value)}/></th>
          <th><input placeholder="Last" type="text" value={this.props.lastFilter} onChange={(event) => this.props.filterChange('lastFilter', event.target.value)}/></th>
          <th>
            <select value={this.props.sideFilter} onChange={(event) => this.props.filterChange('sideFilter', parseInt(event.target.value))}>
              <option value={-1}>Both Sides</option>
              <option value={0}>Lyss</option>
              <option value={1}>Alex</option>
            </select>
          </th>
          <th>Plus One</th>
          <th>Kids</th>
          <td className="edit"/>
        </tr>
        </thead>
        {this.props.invitations.map((invitation, index) => {
          if (this.props.sideFilter !== -1 && invitation.side !== this.props.sideFilter) {
            return null;
          }
          if (!invitation.name.toLowerCase().includes(this.props.nameFilter.toLowerCase())) {
            return null;
          }
          let foundFirst = false;
          let foundLast = false;
          for (const guest of invitation.guests) {
            if (guest.first.toLowerCase().includes(this.props.firstFilter.toLowerCase())) {
              foundFirst = true;
            }
            if (guest.last.toLowerCase().includes(this.props.lastFilter.toLowerCase())) {
              foundLast = true;
            }
          }
          if (!foundFirst || !foundLast) {
            return null;
          }
          return (
            <tbody key={index} className={this.props.checkForAddress(invitation) ? '' : 'missing'}>
            <tr>
              <td>{invitation.name}</td>
              <td>{invitation.guests[0].first}</td>
              <td>{invitation.guests[0].first}</td>
              <td>{invitation.side === 0 ? 'Alyssa' : 'Alex'}</td>
              <td>{(invitation.plusOne) ? 'Yes' : 'No'}</td>
              <td>{invitation.kids}</td>
              <td><Link to={'/edit/' + invitation.id}>edit</Link></td>
            </tr>
            {invitation.guests.map((guest, index) => {
              if (index === 0) {
                return null;
              }
              return (
                <tr key={'sub' + index}>
                  <td/>
                  <td>{guest.first}</td>
                  <td>{guest.last}</td>
                </tr>
              );
            })}
            </tbody>
          );
        })}
      </table>
    );
  }

}

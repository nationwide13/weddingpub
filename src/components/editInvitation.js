import React, { Component } from "react";
import { graphql, compose } from "react-apollo";
import { Link } from "react-router-dom";

import * as Utilities from '../utilities';

import * as Mutations from '../GraphQL/Mutations';
import * as Queries from '../GraphQL/Queries';

import CreateInvitation from './createInvitation';

class editInvitation extends Component {

  emptyGuest = {first: '', last: ''}

  getDefaultItem = () => {
    return {
      name: '',
      count: 0,
      side: 0,
      guests: [this.emptyGuest],
      plusOne: false,
      kids: 0
    }
  };

  addGuest() {
    const item = this.state.item;
    item.guests.push(this.emptyGuest);
    item.count = item.guests.length;
    this.setState({item});
  }

  handleGuestChange(field, index, event) {
    const value = event.target.value.trim();
    const item = this.state.item;
    item.guests[index][field] = value;
    this.setState({item});
  }

  handleFieldChange(field, event) {
    const item = this.state.item;
    if (field === 'date') {
      const date = Utilities.formatDate(event.target.value);
      item.date = date.int;
      this.setState({
        item,
        formattedDate: date.str
      });
    } else {
      if (event.target.type === 'text') {
        item[field] = event.target.value.trim();
      } else if (event.target.type === 'checkbox') {
        item[field] = event.target.checked;
      } else {
        item[field] = event.target.value;
      }
      this.setState({item});
    }
  }

  constructor(props) {
    super(props);
    const date = Utilities.formatDate(new Date());
    this.state = {
      item: this.getDefaultItem(),
      formattedDate: date.str,
      error: ''
    }
  }

  submitForm = () => {
    this.props.createInvitation(this.state.item);
    this.setState({item: this.getDefaultItem()});
  };

  render() {
    if (!this.props.invitation) {
      return null;
    }
    return (
      <CreateInvitation item={this.props.invitation}/>
    );
  }

}

export default compose(
  graphql(
    Queries.getInvitation,
    {
      options: ({ match: { params: { id } } }) => ({
        variables: {id},
        forceFetch: true,
        fetchPolicy: 'cache-and-network',
      }),
      props: (data) => {
        return {invitation: data.data.getInvitation};
      }
    }
  )
)(editInvitation);

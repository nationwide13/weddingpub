import * as React from 'react';
import ReactDOM from 'react-dom';
import JustGage from 'justgage-meteor';

export default class Gauge extends React.Component {
  componentDidMount() {
    this.node = ReactDOM.findDOMNode( this );

    this.guage = new JustGage({
      id: "guage" + this.props.title,
      value: parseInt( this.props.value ),
      min: parseInt( this.props.min ),
      max: parseInt( this.props.max ),
      title: this.props.title,
      label: this.props.label,
      donut: this.props.donut ? this.props.donut : false,
      pointer: true
    });
  }
  componentWillReceiveProps( nextProps ) {
    if ( nextProps.value !== this.props.value ) {
      this.guage.refresh( nextProps.value );
    }
    if ( nextProps.max !== this.props.max ) {
      this.guage.refresh( this.props.value, nextProps.max );
    }
  }
  render() {
    return (
      <div id={'guage' + this.props.title} />
    );
  }
}

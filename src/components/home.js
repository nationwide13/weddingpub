import React, { Component } from "react";
import { NavLink, Route } from "react-router-dom";
import { graphql, compose } from "react-apollo";

import * as Queries from '../GraphQL/Queries';
import * as Mutations from '../GraphQL/Mutations';

import CreateInvitation from './createInvitation';
import invitationList from './invitationList';
import editInvitation from './editInvitation';
import Loading from './loading';
import Gauge from './gauge';

class Home extends Component {

  constructor() {
    super();
    this.state = {
      width: window.innerWidth,
    };
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  render() {
    const ready = !!this.props.invitations;
    return (
      <div className="wedding-shizznit">
        <ul className="navbar">
          <li><NavLink exact={true} to="/">Home</NavLink></li>
          <li><NavLink exact={true} to="/list">Inviation List</NavLink></li>
          <li><NavLink exact={true} to="/create">Add Inviation</NavLink></li>
        </ul>
        {ready ? (
          <div>
            <Route path="/" exact={true} render={() => (
              <div>
                <Gauge value={this.props.acceptedCount} min={0} max={this.props.totalCount} title="Accepted" label="Guests"/>
                <Gauge value={this.props.missingAddress} min={0} max={this.props.invitations.length} title="Missing Addresses" label="Invitations"/>
                <h3>Total Invitations: {this.props.invitations.length}</h3>
                <h3>Max Possible Guests: {this.props.totalCount}</h3>
              </div>
            )}/>
            < Route path="/list" exact={true} component={invitationList}/>
            <Route path="/create" exact={true} component={CreateInvitation}/>
            <Route path="/edit/:id" component={editInvitation}/>
          </div>
        ) : (
          <Loading/>
        )}
      </div>
    );
  }

}

export default compose(
  graphql(
    Queries.listInvitations,
    {
      options: {
        forceFetch: true,
        fetchPolicy: 'cache-and-network',
      },
      props: ({data}) => {
        let missingAddress = 0;
        let acceptedCount = 0;
        if (data.listInvitations) {
          missingAddress = data.listInvitations.filter((invite) => {
            return !(invite.street1 !== null &&
              invite.state !== null &&
              invite.city !== null &&
              invite.zip !== null);

          }).length;
          for (const invite of data.listInvitations) {
            acceptedCount += invite.acceptedCount;
          }
        }
        return {invitations: data.listInvitations, missingAddress, acceptedCount};
      }
    }
  ),
  graphql(
    Queries.getGrandTotalCount,
    {
      options: {
        forceFetch: true,
        fetchPolicy: 'cache-and-network',
      },
      props: (data) => {
        return {totalCount: data.data.getGrandTotalCount};
      }
    }
  ),
  graphql(
    Queries.getAcceptedCount,
    {
      options: {
        forceFetch: true,
        fetchPolicy: 'cache-and-network',
      },
      props: ({data}) => {
        return {acceptedCount: data.getAcceptedCount};
      }
    }
  ),
  graphql(
    Mutations.createInvitation,
    {
      options: {
        refetchQueries: [{ query: Queries.listInvitations }],
        update: (proxy, {data: { createInvitation }}) => {
          const query = Queries.listInvitations;
          const data = proxy.readQuery({ query });
          if (createInvitation) {
            data.listInvitations.push(createInvitation);
          }
          proxy.writeQuery({ query, data });
        }
      },
      props: ({mutate}) => ({
        createInvitation: (input) => {
          return mutate({
            variables: {input: input}
          })
        }
      })
    }
  )
)(Home);

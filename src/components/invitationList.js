import React, { Component } from "react";
import { Link } from "react-router-dom";
import { graphql, compose } from "react-apollo";

import * as Queries from '../GraphQL/Queries';

import DestopInvitationList from './desktopInvitationList';
import MobileInvitationList from './mobileInvitationList';

class invitationList extends Component {

  constructor(props) {
    super(props);
    const savedState = window.localStorage.getItem('filterState');
    const parsedState = savedState ? JSON.parse(savedState) : {};
    this.state = {
      nameFilter: '',
      firstFilter: '',
      lastFilter: '',
      sideFilter: -1,
      missing: false,
      ...parsedState
    }
  }

  checkForAddress = (invite) => {
    return (invite.street1 !== null &&
      invite.state !== null &&
      invite.city !== null &&
      invite.zip !== null);
  }

  filterChange = (target, value) => {
    const state = this.state;
    state[target] = value;
    window.localStorage.setItem('filterState', JSON.stringify(state));
    this.setState(state);
  }

  render() {
    if (!this.props.invitations) {
      return null;
    }
    const state = this.state;
    const mobile = window.innerWidth <= 500;
    return (
      <div className="item-list">
        <h3>Invitations</h3>
        {mobile ?
          <MobileInvitationList
            invitations={this.props.invitations}
            nameFilter={state.nameFilter}
            firstFilter={state.firstFilter}
            lastFilter={state.lastFilter}
            sideFilter={state.sideFilter}
            missing={state.missing}
            checkForAddress={this.checkForAddress}
            filterChange={this.filterChange}
          />
          :
          <DestopInvitationList
            invitations={this.props.invitations}
            nameFilter={state.nameFilter}
            firstFilter={state.firstFilter}
            lastFilter={state.lastFilter}
            sideFilter={state.sideFilter}
            missing={state.missing}
            checkForAddress={this.checkForAddress}
            filterChange={this.filterChange}
          />
        }
      </div>
    );
  }

}

export default compose(
  graphql(
    Queries.listInvitations,
    {
      options: {
        forceFetch: true,
        fetchPolicy: 'cache-and-network',
      },
      props: ({data}) => {
        return {invitations: data.listInvitations};
      }
    }
  )
)(invitationList);

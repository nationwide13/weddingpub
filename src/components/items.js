import React, { Component } from "react";
import { Link } from "react-router-dom";

import { graphql, compose } from "react-apollo";
import QueryGetItems from "../GraphQL/QueryGetItems";
import QeuryGetItemsByCategory from '../GraphQL/QueryGetItemsByCategory';
// import MutationDeleteEvent from "../GraphQL/MutationDeleteEvent";

import moment from "moment";

class AllEvents extends Component {

  render() {
    const catId = this.props.match.params.categoryid;
    const items = (catId) ? this.props.catItems : this.props.items;
    console.log(this.props);

    return (
      <div className="ui link cards">
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Category</th>
              <th>Date</th>
            </tr>
          </thead>
          {items &&
            <tbody>
              {items.map((item, index) => {
                return (
                  <tr key={index}>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td><Link to={'/' + item.category.id}>{item.category.name}</Link></td>
                    <td>{item.date}</td>
                  </tr>
                );
              })}
            </tbody>
          }
        </table>
      </div>
    );
  }

}

export default compose(
  graphql(
    QueryGetItems,
    {
      options: {
        fetchPolicy: 'cache-and-network',
      },
      props: ({ data: { getItems } }) => ({
        items: getItems
      })
    }
  ),
  graphql(
    QeuryGetItemsByCategory,
    {
      options: ({ match: { params: { categoryid } } }) => ({
        variables: { categoryid },
        fetchPolicy: 'cache-and-network',
      }),
      props: ({ data: { getItemsByCategory } }) => ({
        catItems: getItemsByCategory
      })
    }
  )
)(AllEvents);

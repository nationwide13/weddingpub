import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class MobileInvitaitonList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      popupDetails: {},
      filters: false
    }
  }

  render() {
    if (!this.props.invitations) {
      return null;
    }
    const popupDetails = this.state.popupDetails;
    return (
      <div className="mobile">
        <div className="filterToggle" onClick={() => this.setState({filters: !this.state.filters})}>Filters</div>
        <div className={`filters ${this.state.filters ? 'active' : ''}`}>
          <label>Name: <input placeholder="Name" type="text" value={this.props.nameFilter} onChange={(event) => this.props.filterChange('nameFilter', event.target.value)}/></label>
          <label>First: <input placeholder="First" type="text" value={this.props.firstFilter} onChange={(event) => this.props.filterChange('firstFilter', event.target.value)}/></label>
          <label>Last: <input placeholder="Last" type="text" value={this.props.lastFilter} onChange={(event) => this.props.filterChange('lastFilter', event.target.value)}/></label>
          <label>Side:
            <select value={this.props.sideFilter} onChange={(event) => this.props.filterChange('sideFilter', parseInt(event.target.value))}>
              <option value={-1}>Both Sides</option>
              <option value={0}>Lyss</option>
              <option value={1}>Alex</option>
            </select>
          </label>
          <label>Only Invites Missing Addresses: <input type="checkbox" onChange={(event) => this.props.filterChange('missing', event.target.checked)}/></label>
        </div>
        <table>
          <tbody >
            {this.props.invitations.map((invitation, index) => {
              if (this.props.sideFilter !== -1 && invitation.side !== this.props.sideFilter) {
                return null;
              }
              if (!invitation.name.toLowerCase().includes(this.props.nameFilter.toLowerCase())) {
                return null;
              }
              if (this.props.missing && this.props.checkForAddress(invitation)) {
                return null;
              }
              let foundFirst = false;
              let foundLast = false;
              for (const guest of invitation.guests) {
                if (guest.first.toLowerCase().includes(this.props.firstFilter.toLowerCase())) {
                  foundFirst = true;
                }
                if (guest.last.toLowerCase().includes(this.props.lastFilter.toLowerCase())) {
                  foundLast = true;
                }
              }
              if (!foundFirst || !foundLast) {
                return null;
              }
              return (
                  <tr key={index} onClick={() => this.setState({popupDetails: invitation})} className={this.props.checkForAddress(invitation) ? '' : 'missing'}>
                    <td>{invitation.name}</td>
                    <td className="right">{invitation.guests[0].first}</td>
                  </tr>
              );
            })}
          </tbody>
        </table>
        {popupDetails.name &&
        <div className="popup">
          <div className="close" onClick={() => this.setState({popupDetails: {}})}>X</div>
          <div>
            <div>{popupDetails.name}</div>
            <div>Side: {popupDetails.side === 0 ? 'Alyssa' : 'Alex'}</div>
            {popupDetails.street1 ? (
              <div>
                <div>{popupDetails.street1}</div>
                {popupDetails.street2 && <div>{popupDetails.street2}</div>}
                <div>{popupDetails.city}, {popupDetails.state} {popupDetails.zip}</div>
              </div>
            ) : (
              <div>Address Needed!</div>
            )}
          </div>
          <br/>
          <div>Guests:</div>
          <table>
            <tbody>
              {popupDetails.guests.map((guest, index) => {
                return (
                  <tr key={'sub' + index}>
                    <td>{guest.first}</td>
                    <td>{guest.last}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <br/>
          <div>Plus One: {(popupDetails.plusOne) ? 'Yes' : 'No'}</div>
          <div>Kids: {popupDetails.kids}</div>
          <br/>
          <Link to={'/edit/' + popupDetails.id}>edit</Link>
        </div>
        }
        {popupDetails.name && <div className="overlay" onClick={() => this.setState({popupDetails: {}})}/>}
      </div>
    );
  }
}

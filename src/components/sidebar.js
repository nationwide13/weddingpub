import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class SideBar extends Component {
  render() {
    return (
      <div className={`sidebar ${this.props.active}`}>
        <div className="content">
          <h3>Categories</h3>
          <ul>
            {this.props.cats.map((cat, index) => {
              return (
                <li key={index} className={cat.id === this.props.cat ? 'selected' : ''}>{cat.name}</li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }

}

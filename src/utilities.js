export const formatDate = (date) => {
  if (date.getMonth) {
    let month = date.getMonth() + 1;
    let day = date.getDate();
    if (month < 10) {
      month = '0' + month;
    }
    if (day < 10) {
      day = '0' + day;
    }
    return {
      str: date.getFullYear() + '-' + month + '-' + day,
      int: 1 * (date.getFullYear() + month + day)
    }
  } else if (date.length) {
    return {
      str: date,
      int: (date.slice(0,4) + date.slice(5,7) + date.slice(8,10)) * 1
    }
  } else {
    date = date.toString();
    return {
      str: date.slice(0,4) + '-' + date.slice(4,6) + '-' + date.slice(6,8),
      int: date
    }
  }
}
